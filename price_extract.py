#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json

import pymysql

hostip = 'DataContexts.cqb1mv8v62cx.eu-north-1.rds.amazonaws.com'
user = 'randd'
password = 'szrlynudl6l9gpywdktn'
database = 'affiliate_sales'
responseObject = {}


# Main function
def lambda_handler(event, context):
    con = pymysql.connect(host=f'{hostip}',
                          user=f'{user}',
                          password=f'{password}',
                          database=f'{database}')
    not_in_stock = ('out of stock', '-1', 'Nej', 'nej', 0)
    if not 'sort' in event:
        event['sort'] = {}
        event['sort']['price'] = "ASC"
    if not 'limit' in event:
        event['limit'] = 20
    if not 'page' in event:
        event['page'] = 0
    if not 'category' in event:
        event['category'] = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29"
    if not 'sub_category' in event:
        event['sub_category'] = "''"

    limit = event['limit']
    page = event['page']
    order = event['sort']['price']
    category_id = event['category']
    sub_category_id = event['sub_category']
    cursor = con.cursor()
    sql_query = f"SELECT m1.product_id, a2.price,  "\
                    f"CASE WHEN JSON_VALID(a2.product_data) THEN JSON_UNQUOTE(JSON_EXTRACT(a2.product_data, " \
                        f"'$.meta.stockAmount')) ELSE NULL END AS stock " \
	                f"FROM meta.products as m1 " \
		            f"JOIN affiliate_sales.scrapes_to_products a1 " \
	                    f"ON m1.id = a1.product_id " \
		            f"RIGHT JOIN affiliate_sales.scrapes a2 " \
	                    f"ON a1.scrape_id = a2.id " \
		            f"WHERE m1.product_id IS NOT null " \
	                    f"AND a2.price IS NOT NULL " \
	                    f"AND (m1.sub_category_id IN ({sub_category_id}) OR category_id IN ({category_id})) " \
                    f"ORDER BY a2.price {order} LIMIT {page}, {limit} "
    cursor.execute(sql_query)
    result = list(cursor.fetchall())
    for id, price, stock in result:
        responseObject[id] = {}
        if stock in not_in_stock:
            responseObject[id]['stock'] = 'Out Of Stock'
        else:
            responseObject[id]['stock'] = 'In Stock'
        price = float(price)
        responseObject[id]['price'] = price
    return responseObject




event = {}

print(lambda_handler(event, event))